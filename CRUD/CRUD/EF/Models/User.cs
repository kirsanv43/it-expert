﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRUD.EF.Models
{
    public class User
    {
        [Key]
        public int user_id { get; set; }
        public string nickname { get; set; }
        public int department_id { get; set; }
        public int person_id { get; set; }
        public int position_id { get; set; }
        public bool super_user { get; set; }
    }
}